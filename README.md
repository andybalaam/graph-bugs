Ants that draw graphs!

Play online at [artificialworlds.net/kidsgames/graph-bugs/graph-bugs.html](http://artificialworlds.net/kidsgames/graph-bugs/graph-bugs.html)

![Graph Bugs drawing y=sin(x)](graph-bugs.gif)

## Copyright

Copyright 2015 Andy Balaam, released under the AGPLv3 licence. See
[LICENSE](LICENSE) for details.

The ant image is from
https://opengameart.org/content/walking-ant-with-parts-and-rigged-spriter-file,
licensed under [CC0](http://creativecommons.org/publicdomain/zero/1.0/)
